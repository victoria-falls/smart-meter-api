# Smart Meter API

A simple NodeJS backend w. RESTful API.
It acts as an intermediate for interaction with the underlying MongoDB database.

One would access the api via: 'https://yourdomain.net/api/:meterID'
Presented with a JSON dump of requested data.

Supports querying by:
- MeterID
- Date intervals, since date, range of dates.



